package com.example.fau;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Rubrik extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rubrik);

        CardView card1 = (CardView) findViewById(R.id.cardTop);
        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i3 = new Intent();
                i3.setAction(Intent.ACTION_VIEW);
                i3.addCategory(Intent.CATEGORY_BROWSABLE);
                i3.setData(Uri.parse("https://brilicious.brilio.net/kuliner-kesehatan/25-makanan-enak-yang-cocok-untuk-program-diet-1811074.html"));
                startActivity(i3);
            }
        });

        CardView card2 = (CardView) findViewById(R.id.cardView4);
        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent();
                i2.setAction(Intent.ACTION_VIEW);
                i2.addCategory(Intent.CATEGORY_BROWSABLE);
                i2.setData(Uri.parse("https://www.idntimes.com/food/dining-guide/muyassaroh/5-jenis-makanan-sehat-yang-terbukti-ampuh-hilangkan-stres-c1c2/full"));
                startActivity(i2);
            }
        });

        CardView card3 = (CardView) findViewById(R.id.card3);
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i1 = new Intent();
                i1.setAction(Intent.ACTION_VIEW);
                i1.addCategory(Intent.CATEGORY_BROWSABLE);
                i1.setData(Uri.parse("https://kumparan.com/@kumparansains/4-tips-makan-sehat-dengan-budget-anak-kos?ref=bcjuga"));
                startActivity(i1);
            }
        });
    }
}
