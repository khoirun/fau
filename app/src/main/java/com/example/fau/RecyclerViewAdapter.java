package com.example.fau;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

<<<<<<< HEAD
import com.example.fau.DetailActivity;
import com.example.fau.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter <RecyclerViewAdapter.MyHolder>{

    String nama [] = {"Nasi Goreng Bang Japra", "Ayam Madu si Bangkong", "Warung Nasi Warbhie ", "Wisata Kuliner", "Pantai Tanjung Pasir"};
    int gambar [] = {R.drawable.takoyaki,
            R.drawable.takoyaki,
            R.drawable.takoyaki,
            R.drawable.takoyaki,
            R.drawable.takoyaki};
    String detail [] = {"Warung nasi goreng yang terletak di daerah umayyah 2. Buka mulai dari jam 6 sore hingga 1 dini hari. \n\n"+
            "Nasgor Biasa \n" + "idr 12.000 \n\n"+
            "Nasgor special \n" + "idr 20.000 \n\n"+
            "Nasgor gila \n" + "idr 17.000 \n\n" +
            "Nasgor Ati Ampela \n" + "idr 15.000 \n\n"+
            "Nasgor Kampoeng \n" + "idr 14.000 \n\n"+
            "Pemesanan dapat dilakukan melalui tlp/wa/sms/line : 0855 2090 2390",
            "Menu yang tersedia \n\n" +
            "Ayam Madu + Nasi \n"+"idr 15.000 \n\n"+
            "Ayam Madu \n"+"idr 12.000 \n\n"+
            "Ayam Geprek + Nasi \n"+"idr 15.000 \n\n"+
            "Pemesanan dapat dilakukan melalui wa: 0812 2477 2191",
            "Warung ini terletak di jl. Babakan Ciamis 1. Berikut adalah menu-menu yang ada di warung warbhie : \n\n"+
            "Nasi Ayam Cola + Tumis + Sambal (lalapan) \n" + "idr 14.000 \n\n"+
            "Nasi Ayam Katsu + Tumis + Sambal (lalapan) \n"+"idr 14.000 \n\n"+
            "Nasi Ayam Teriyaki + Tumis + Sambal (lalapan) \n"+"idr 14.000 \n\n"+
            "Nasi Ayam Geprek + Tumis + Sambal (lalapan) \n"+"idr 14.000 \n\n"+
            "Delivery Order : 0812 2329 8378",
            "Wisata Kuliner hanya buka pada malam hari dan terletak di Pasar Lama",
            "Pantai Tanjung Pasir terletak di kecamatan Teluk Naga"};
=======

public class RecyclerViewAdapter extends RecyclerView.Adapter <RecyclerViewAdapter.MyHolder>{

    String nama [] = {"Takoyaki"};
    int gambar [] = {R.drawable.takoyaki};
    String detail [] = {"Kedai kecil Takoyaki terletak disamping Alfamart"};
>>>>>>> 811039ec3f607a228e438fa5b899f438bb6a90e7

    Context context;
    LayoutInflater layoutInflater;

    public RecyclerViewAdapter (Context context ){

        this.context = context;
        layoutInflater = layoutInflater.from(context);

    }
    @Override
    public RecyclerViewAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.list_item, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.MyHolder holder, final int position) {

        // ini berfungsi untuk mengirim data ke DetailActivity
        holder.txt.setText(nama [position]);
        holder.img.setImageResource(gambar [position]);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kirimData = new Intent(context, DetailActivity.class);
                kirimData.putExtra("Nama", nama [position]);
                kirimData.putExtra("IMG", gambar [position]);
                kirimData.putExtra("DET", detail [position]);

                context.startActivity(kirimData);

            }
        });
    }

    @Override
    public int getItemCount() {
        return nama.length;
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        // ID ini diambil dari file list_item.xml
        ImageView img;
        TextView txt;

        public MyHolder(View itemView) {

            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imglist);
            txt = (TextView) itemView.findViewById(R.id.txtlist);
        }
    }
}